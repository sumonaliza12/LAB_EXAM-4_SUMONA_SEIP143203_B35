<html>
<head>
    <title>Prime Number</title>
</head>
<body>
<script type="text/javascript">

    function IsPrime(number) {

        if (number < 2)

            return false;


        var value = Math.floor(Math.sqrt(number));
        var i, j;

        for ( i = 2; i <= number; i++)
        {
            if (number % i == 0)
            {
                return false;
            }
        }

        return true;

    }
    document.write("List of Prime Number 1 to 100: <br>")
    for (var i = 1; i <= 100; i++) {
        if(IsPrime(i)==true) {
            document.write(i+"<br>");
        }

    }

</script>
</body>
</html>